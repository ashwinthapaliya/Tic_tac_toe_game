﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace tic_tac_toe
{
    public partial class Form1 : Form
    {
        bool turn = true; //true is X and false is Y
        int count = 0;
        public Form1()
        {
            InitializeComponent();
            Restart_game();
        }

        private void button_click(object sender, EventArgs e)
        {
            Button b = (Button)sender;
            b.Text = (turn) ? "X" : "O";
            turn = !turn;
            ++count;
            b.Enabled = false;

            if (Check_for_winer())
            {
                string winner = "";
                winner = turn ? "O" : "X";
                Disable_game();
                MessageBox.Show("Congratulations ! " + winner + ", Wins !", "Game Over ! " + winner + ", Wins !", MessageBoxButtons.OK, MessageBoxIcon.Information);
                Play_Again();
            };

            if(count==9)
            {
               DialogResult d= MessageBox.Show("The game is held in draw. Do you want to play again?", "Draw game", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
               if (d == DialogResult.Yes)
                {
                    Restart_game();
                }
                else
                {
                    Application.Exit();
                }
            }
        }

        private bool Check_for_winer()
        {
            //check for horizontal align

            if (A1.Text == A2.Text && A2.Text == A3.Text && !A1.Enabled)
            {
                A1.BackColor = A2.BackColor = A3.BackColor = Color.Green;
                return true;
            }
            else if (B1.Text == B2.Text && B2.Text == B3.Text && !B1.Enabled)
            {
                B1.BackColor = B2.BackColor = B3.BackColor =  Color.Green;
                return true;
            }
            else if (C1.Text == C2.Text && C2.Text == C3.Text && !C1.Enabled)
            {
                C1.BackColor = C2.BackColor = C3.BackColor = Color.Green;
                return true;
            }
            //check for vertical
            else if (A1.Text == B1.Text && B1.Text == C1.Text && !A1.Enabled)
            {
                A1.BackColor = B1.BackColor = C1.BackColor = Color.Green;
                return true;
            }
            else if (A2.Text == B2.Text && B2.Text == C2.Text && !A2.Enabled)
            {
                A2.BackColor = B2.BackColor = C2.BackColor = Color.Green;
                return true;
            }
            else if (A3.Text == B3.Text && B3.Text == C3.Text && !A3.Enabled)
            {
                A3.BackColor = B3.BackColor = C3.BackColor = Color.Green;
                return true;
            }
            //check fpr diagonal

            else if (A1.Text == B2.Text && B2.Text == C3.Text && !A1.Enabled)
            {
                A1.BackColor = B2.BackColor = C3.BackColor = Color.Green;
                return true;
            }
            else if (C1.Text == B2.Text && B2.Text == A3.Text && !C1.Enabled)
            {
                C1.BackColor = B2.BackColor = A3.BackColor = Color.Green;
                return true;
            }

            else
                return false;
        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Created and Designed By Ashwin Thapaliya. 2017", "About Developer", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void newGameToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Restart_game();
        }

        private void Restart_game()
        {
            turn = true;
            count = 0;
            try
            {
                foreach (Control c in Controls)
                {
                    Button b = (Button)c;
                    b.Text = "";
                    b.BackColor = Color.White;
                    b.Enabled = true;
                }
            }
            catch { }
           
        }

        private void Disable_game()
        {
            turn = true;
            count = 0;
            try
            {
                foreach (Control c in Controls)
                {
                    Button b = (Button)c;
                    b.Enabled = false;
                }
            }
            catch { }

        }

        private void Play_Again()
        {
            DialogResult d = MessageBox.Show("Want to play Again?", "Restart Game", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if(d==DialogResult.Yes)
            {
                Restart_game();
            }
            else
            {
                Application.Exit();
            }
        }
    }
}
